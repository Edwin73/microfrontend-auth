import {useState} from 'react';

const useForm = (initialState = {}) => {

    const [formValues, setFormValues] = useState(initialState);

    const resetForm = (newFormState = initialState) => {
        setFormValues(initialState);
    }

    const formInputChange = ({target}) => {
        setFormValues((formState) => {
            return {
                ...formState,
                [target.name]: target.value
            }
        });
    }

    return [
        formValues,
        formInputChange,
        resetForm,
    ];
}

export default useForm
