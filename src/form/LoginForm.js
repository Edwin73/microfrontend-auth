import React from "react";
import useForm from "../hooks/useForm";

const LoginForm = () => {
    const [formValues, formInputChange, resetForm] = useForm({
        email: 'edwin@gmail.com',
        password: '12345678',
    });

    const {email, password} = formValues

    const handleLogin = (event) => {
        event.preventDefault();
        console.log(formValues)
    }

    return (
        <form onSubmit={handleLogin}>
            <div className="form-group">
                <label htmlFor="exampleInputEmail1">Email</label>
                <input
                    id="email"
                    type="text"
                    name="email"
                    className="form-control"
                    placeholder="EJ: USER ABC"
                    autoComplete="off"
                    value={email}
                    onChange={formInputChange}
                />
                <small id="emailHelp" className="form-text text-muted">
                    Ingrese el email.
                </small>
            </div>
            <div className="form-group mt-5">
                <label htmlFor="password">Contraseña</label>
                <input
                    type="password"
                    className="form-control"
                    id="password"
                    name="password"
                    aria-describedby="passwordHelp"
                    placeholder="Enter password"
                    value={password}
                    onChange={formInputChange}
                />
                <small id="emailHelp" className="form-text text-muted">
                    Ingrese su contraseña.
                </small>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
    )
}

export default LoginForm
