import React from 'react';
import ReactDom from 'react-dom'
import App from './App';

// montar funcion para iniciar la app
const AuthReactComponent = (element) => {
    ReactDom.render(
        <App/>,
        element,
    );
}

// llamar al mount si estamos ejecutando en dev y aislamiento
if (process.env.NODE_ENV === 'development') {
    const devRoot = document.querySelector('#__auth_app');
    if (devRoot) {
        AuthReactComponent(devRoot);
    }
}

// corremos a traves del container y exportamos la funcion para montar la app
export {AuthReactComponent};
