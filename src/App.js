import React from "react";
import {Switch, Route, BrowserRouter as Router, Redirect,} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';

import Login from './pages/Login'

const App = () => (
        <Router>
            <div>
                <Switch>
                    <Route path="/login" component={Login}/>
                    <Redirect
                        to="/login"
                    />
                </Switch>
            </div>
        </Router>
)

export default App
